#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const char* ssid = "PLDTHOMEFIBR09af0";
const char* password = "Angel12345678!"; 
String serverUrl = "http://192.168.1.107:5000/receive_audio"; 

void setup() {
  Serial.begin(9600);
  delay(10);

  // Connect to WiFi
  WiFi.begin(ssid, password);
  int attemptCounter = 0;

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
    attemptCounter++;

    if (attemptCounter > 10) {
      Serial.println("Failed to connect to WiFi. Please check your credentials and restart.");
      return;
    }
  }

  Serial.println("Connected to WiFi");

  // Indicator: IoT is trying to connect to the server
  Serial.println("Attempting to connect to the server...");
}

void loop() {
  // Assuming audio data size is 1024 bytes
  if (Serial.available() >= 1024) {
    byte audioData[1024];
    Serial.readBytes(audioData, sizeof(audioData));

    // Display received audio data on Serial Monitor
    Serial.println("Received Audio Data:");
    for (size_t i = 0; i < sizeof(audioData); i++) {
      Serial.print(audioData[i]);
      Serial.print(" ");
    }
    Serial.println(); 

    WiFiClient client;
    HTTPClient http;

    http.begin(client, serverUrl);
    http.addHeader("Content-Type", "application/octet-stream");

    // Pass the byte array directly to POST method
    int httpResponseCode = http.POST(audioData, sizeof(audioData));

    if (httpResponseCode > 0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);

      // Indicator: IoT successfully connected to the server
      Serial.println("Successfully connected to the server!");

      // Add indicator for data transmission success
      if (httpResponseCode == HTTP_CODE_OK) {
        Serial.println("Data transmitted successfully!");
      } else {
        Serial.println("Failed to transmit data. Check server response for details.");
      }
    } else {
      Serial.print("HTTP POST request failed. Error code: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  }
}
