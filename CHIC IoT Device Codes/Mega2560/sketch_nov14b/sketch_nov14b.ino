#include <SoftwareSerial.h>

SoftwareSerial espSerial(17, 18);  // RX, TX pins for ESP8266 communication
#define captureDuration 5000  // 5 seconds in milliseconds
#define captureInterval 1000  // Capture data every 1 second

void setup() {
  Serial.begin(9600);
  espSerial.begin(9600);  // Start communication with ESP8266
  delay(10);  // Allow time for serial communication to establish
}

void loop() {
  unsigned long startTime = millis();
  unsigned long elapsedTime;

  Serial.println("Starting data capture...");

  // Check for WiFi connection indicator from ESP8266
  if (espSerial.available() > 0) {
    String indicator = espSerial.readStringUntil('\n');
    if (indicator == "WiFiConnected") {
      Serial.println("ESP8266 is connected to WiFi.");
    } else {
      Serial.println("Failed to connect to WiFi. Check ESP8266 for details.");
      return;
    }
  }

  // Indicator: Mega 2560 is starting to send data
  Serial.println("Sending data to server...");

  while ((elapsedTime = millis() - startTime) < captureDuration) {
    // Assuming audio data size is 1024 bytes
    byte audioData[1024];
    readAudioData(audioData, sizeof(audioData));

    // Send data to ESP8266 over serial
    espSerial.write(audioData, sizeof(audioData));

    // Display progress indicator
    Serial.print(".");
    
    // Display the captured data on Serial Monitor
    Serial.println("Captured Audio Data:");
    for (size_t i = 0; i < sizeof(audioData); i++) {
      Serial.print(audioData[i]);
      Serial.print(" ");
    }
    
    Serial.println();  // Move to the next line for clarity

    delay(10);  // Delay to control the frequency of data transmission
  }

  // Reset the timer for the next capture
  delay(captureInterval);

  // Indicator: Mega 2560 has completed sending data
  Serial.println("Data capture complete. Waiting for the next capture...");
}

void readAudioData(byte* audioData, size_t dataSize) {
  for (size_t i = 0; i < dataSize; i++) {
    // Read analog data from MAX9814
    int analogValue = analogRead(A0); 

    // Map the analog value (0-1023) to byte (0-255)
    audioData[i] = map(analogValue, 0, 1023, 0, 255);
  }
}
