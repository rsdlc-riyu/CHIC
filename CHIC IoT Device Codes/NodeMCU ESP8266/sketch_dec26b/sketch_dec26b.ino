#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const char* ssid = "GlobeAtHome_A9DC1";
const char* password = "7JH3LB19QAE";

String serverBaseUrl = "http://192.168.254.121:5000/receive_audio/";

#define captureDuration 5000  // 5 seconds in milliseconds
#define captureInterval 1000  // Capture data every 1 second

// Function declarations
void sendToServer(byte* data, size_t dataSize, String serverUrl);
void captureAndSendData(int microphonePin, const char* microphoneName);

void setup() {
  Serial.begin(115200);
  delay(100); // Added delay for initialization

  // Connect to WiFi
  WiFi.begin(ssid, password);
  int attemptCounter = 0;

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("Connecting to WiFi, attempt number: ");
    Serial.println(attemptCounter + 1);

    attemptCounter++;

    if (attemptCounter > 10) {
      Serial.println("Failed to connect to WiFi. Please check your credentials and restart.");
      return;
    }
  }

  Serial.println("Connected to WiFi");
}

void loop() {
  // Capture audio from microphone 1
  captureAndSendData(D0, "Microphone_1");

  // Capture audio from microphone 2
  captureAndSendData(D1, "Microphone_2");
}

void captureAndSendData(int microphonePin, const char* microphoneName) {
  unsigned long startTime = millis();
  unsigned long elapsedTime;

  Serial.print("Capturing data from ");
  Serial.println(microphoneName);

  // Construct the complete server URL including the modified microphone name
  String serverUrl = serverBaseUrl + microphoneName;

  byte audioData[1024 * captureDuration / captureInterval];  // Buffer to accumulate audio data

  size_t dataIndex = 0;  // Index to keep track of the position in the audioData buffer

  while ((elapsedTime = millis() - startTime) < captureDuration) {
    int analogValue = analogRead(microphonePin);
    byte mappedValue = map(analogValue, 0, 1023, 0, 255);

    audioData[dataIndex++] = mappedValue;

    Serial.print(".");
    
    delay(10);
  }

  Serial.println();
  Serial.println("Data capture complete. Waiting for the next capture...");

  // Send the accumulated audio data
  sendToServer(audioData, dataIndex, serverUrl);

  // Delay before switching to the next microphone
  delay(captureInterval);
}

void readAudioData(byte* audioData, size_t dataSize, int microphonePin) {
  for (size_t i = 0; i < dataSize; i++) {
    int analogValue = analogRead(microphonePin);
    audioData[i] = map(analogValue, 0, 1023, 0, 255);
  }
}

void sendToServer(byte* data, size_t dataSize, String serverUrl) {
  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverUrl);
  http.addHeader("Content-Type", "application/octet-stream");

  int httpResponseCode = http.POST(data, dataSize);

  if (httpResponseCode > 0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);

    Serial.println("Successfully connected to the server!");

    if (httpResponseCode == HTTP_CODE_OK) {
      Serial.println("Data transmitted successfully!");
    } else {
      Serial.println("Failed to transmit data. Check server response for details.");
    }
  } else {
    Serial.print("HTTP POST request failed. Error code: ");
    Serial.println(httpResponseCode);
  }

  http.end();
}
