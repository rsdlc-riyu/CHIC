#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const char *ssid = "PLDTHOMEFIBR09af0";
const char *password = "Angel12345678!";

String serverBaseUrl = "http://192.168.1.107:5000/receive_audio/";

#define captureDuration 5000    // 5 seconds in milliseconds
#define captureInterval 62    // Capture data every 1 second
#define elementSize sizeof(byte) // Size of the data type used in the buffer
#define bufferSize (1024 * captureDuration / (captureInterval * elementSize))

// Function declarations
void sendToServer(byte *data, size_t dataSize, String serverUrl, String microphoneName);
void captureAndSendData(int microphonePin, String microphoneName);

void setup()
{
  Serial.begin(115200);
  delay(100);

  // Connect to WiFi
  WiFi.begin(ssid, password);
  int attemptCounter = 0;

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print("Connecting to WiFi, attempt number: ");
    Serial.println(attemptCounter + 1);

    attemptCounter++;

    if (attemptCounter > 10)
    {
      Serial.println("Failed to connect to WiFi. Please check your credentials and restart.");
      return;
    }
  }

  Serial.println("Connected to WiFi");
}

void loop()
{
  // Capture audio from microphone 1
  captureAndSendData(D0, "Microphone_1");

  // Wait for the server to process Microphone_1 data
  delay(captureInterval * 2);

  // Capture audio from microphone 2
  captureAndSendData(D1, "Microphone_2");
}

void captureAndSendData(int microphonePin, String microphoneName)
{
  unsigned long startTime = millis();
  unsigned long elapsedTime;

  Serial.print("Capturing data from ");
  Serial.println(microphoneName);

  byte audioData[bufferSize]; // Use the adjusted buffer size

  size_t dataIndex = 0; // Index to keep track of the position in the audioData buffer

  while ((elapsedTime = millis() - startTime) < captureDuration)
  {
    int analogValue = analogRead(microphonePin);

    // Directly store the mapped value into audioData
    audioData[dataIndex++] = map(analogValue, 0, 1023, 0, 255);

    Serial.print(".");

    // Calculate the remaining time and adjust delay accordingly
    unsigned long remainingTime = captureDuration - elapsedTime;
    unsigned long iterationTime = min(remainingTime, (unsigned long)10);
    
    delay(iterationTime);
  }

  Serial.println();
  Serial.print("Data capture complete. dataIndex: ");
  Serial.println(dataIndex);

  // Calculate the RATE
  unsigned long numberSamples = dataIndex;
  unsigned long RATE = numberSamples / (captureDuration / 1000); // Convert captureDuration to seconds
  Serial.print("Calculated RATE: ");
  Serial.println(RATE);

  // Send the accumulated audio data as RAW using multipart/form-data
  sendToServer(audioData, dataIndex, serverBaseUrl, microphoneName);
}

void sendToServer(byte *data, size_t dataSize, String serverUrl, String microphoneName)
{
  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverUrl + microphoneName);

  // Set the correct Content-Type for raw data
  http.addHeader("Content-Type", "application/octet-stream");

  // Send the audio data directly
  int httpResponseCode = http.sendRequest("POST", data, dataSize);

  // Handle the server response
  if (httpResponseCode == HTTP_CODE_OK)
  {
    Serial.println("Data transmitted successfully!");
  }
  else
  {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    Serial.println("Failed to transmit data. Check server response for details.");
  }

  http.end();
}
