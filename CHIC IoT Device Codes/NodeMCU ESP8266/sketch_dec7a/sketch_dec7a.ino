#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const char* ssid = "GlobeAtHome_A9DC1";
const char* password = "7JH3LB19QAE";

String serverUrl = "http://192.168.254.121:5000/receive_audio";

#define captureDuration 5000  // 5 seconds in milliseconds
#define captureInterval 1000  // Capture data every 1 second

void setup() {
  Serial.begin(115200);
  delay(100); // Added delay for initialization

  // Connect to WiFi
  WiFi.begin(ssid, password);
  int attemptCounter = 0;

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("Connecting to WiFi, attempt number: ");
    Serial.println(attemptCounter + 1);

    attemptCounter++;

    if (attemptCounter > 10) {
      Serial.println("Failed to connect to WiFi. Please check your credentials and restart.");
      return;
    }
  }

  Serial.println("Connected to WiFi");
}

void loop() {
  unsigned long startTime = millis();
  unsigned long elapsedTime;

  Serial.println("Starting data capture...");

  while ((elapsedTime = millis() - startTime) < captureDuration) {
    // Assuming audio data size is 1024 bytes
    byte audioData[1024];
    readAudioData(audioData, sizeof(audioData));

    // Display progress indicator
    Serial.print(".");

    // Send data to the server
    sendToServer(audioData, sizeof(audioData));

    delay(10);  // Delay to control the frequency of data transmission
  }

  // Reset the timer for the next capture
  delay(captureInterval);

  // Indicator: ESP8266 has completed sending data
  Serial.println("Data capture complete. Waiting for the next capture...");
}

void readAudioData(byte* audioData, size_t dataSize) {
  for (size_t i = 0; i < dataSize; i++) {
    // Read analog data from MAX9814 connected to A0 pin
    int analogValue = analogRead(A0);

    // Map the analog value (0-1023) to byte (0-255)
    audioData[i] = map(analogValue, 0, 1023, 0, 255);
  }
}

void sendToServer(byte* data, size_t dataSize) {
  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverUrl);
  http.addHeader("Content-Type", "application/octet-stream");

  // Pass the byte array directly to POST method
  int httpResponseCode = http.POST(data, dataSize);

  if (httpResponseCode > 0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);

    // Indicator: ESP8266 successfully connected to the server
    Serial.println("Successfully connected to the server!");

    // Add indicator for data transmission success
    if (httpResponseCode == HTTP_CODE_OK) {
      Serial.println("Data transmitted successfully!");
    } else {
      Serial.println("Failed to transmit data. Check server response for details.");
    }
  } else {
    Serial.print("HTTP POST request failed. Error code: ");
    Serial.println(httpResponseCode);
  }

  http.end();
}
