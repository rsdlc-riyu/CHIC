import csv
import os
import wave
import numpy as np
import tensorflow as tf
import uuid
import threading
import librosa
from flask import Flask, flash, jsonify, render_template, request, g, redirect, url_for
from flask_cors import CORS
from flask_socketio import SocketIO
import sqlite3
from datetime import datetime
from scipy import signal
from scipy.io.wavfile import write
from twilio.rest import Client
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, EqualTo
from scipy.io.wavfile import write as wav_write

app = Flask(__name__)
app.config['DATABASE_DISTRESS'] = 'distress_history.db'
app.config['DATABASE_CALENDAR'] = 'calendar_data.db'
app.config['SECRET_KEY'] = '2b6a52ccf29b0b826a3c63efb939d43faa7edd279584f75e0d8c76dfe328d1d3'
socketio = SocketIO(app, cors_allowed_origins='*')
CORS(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'

# # Twilio configuration
# twilio_account_sid = 'AC1fefac9e88b20e2ac42e21041171fa31'
# twilio_auth_token = '7c627df53cb592911c0742c0e096c180'
# twilio_phone_number = '+15598920067'

# client = Client(twilio_account_sid, twilio_auth_token)

# # Fixed recipient number for Twilio SMS
# fixed_recipient_number = '+639603112083' 

temp_dir = 'temp_files'
os.makedirs(temp_dir, exist_ok=True)

conn_distress = sqlite3.connect(app.config['DATABASE_DISTRESS'], check_same_thread=False)
cursor_distress = conn_distress.cursor()

cursor_distress.execute('''
    CREATE TABLE IF NOT EXISTS history_log (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        datetime DATETIME NOT NULL,
        distress_type TEXT NOT NULL,
        microphone_number TEXT NOT NULL
    )
''')
conn_distress.commit()

conn_calendar = sqlite3.connect(app.config['DATABASE_CALENDAR'], check_same_thread=False)
cursor_calendar = conn_calendar.cursor()

model_path = 'ChickenDistressVocalization1.h5'

try:
    model = tf.keras.models.load_model(model_path)
except Exception as e:
    print(f"Error loading the model: {e}")
    # Handle the error appropriately, e.g., raise an exception or exit the application

model = tf.keras.models.load_model(model_path)
lock = threading.Lock()

CHANNELS = 1
RATE = 22050
# Calculate the maximum number of frames based on the desired duration
DESIRED_DURATION = 5.0  # Desired duration in seconds
MAX_FRAMES = int(DESIRED_DURATION * RATE)



classes = {
    0: "Normal",
    1: "Chicken is Disturbed",
    2: "Chicken is in Danger",
    3: "Chicken is Threatened",
    4: "Background Noise",
}

# Initialize general distress counts
general_distress_counts = {
    "Chicken is Disturbed": 0,
    "Chicken is in Danger": 0,
    "Chicken is Threatened": 0
}

microphone_distress_counts = {}

# User class for login management
class User(UserMixin):
    def __init__(self, user_id):
        self.id = user_id

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

# Define login form
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')


@app.route('/')
def home():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    return render_template('index.html', form=LoginForm())


@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html', username=current_user.id)

# Add login route
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        # Authenticate the user
        if form.username.data == 'Admin' and form.password.data == 'chicadmin123':
            user = User(form.username.data)
            login_user(user)
            return redirect(url_for('dashboard'))
        else:
            flash('Invalid username or password. Please try again.', 'danger')
    return render_template('index.html', form=form)

# Add logout route
@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/audio_analysis')
def audio_analysis():
    return render_template('audio_analysis.html')

@app.route('/history_logs')
def history_logs():
    return render_template('history.html')
    
@app.route('/observations')
def observations():
    return render_template('observations.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/team')
def team():
    return render_template('team.html')


# def send_twilio_sms(distress_type, microphone_number, recipient_number):
#     try:
#         # Customize the SMS message based on distress type and microphone number
#         sms_message = f"Alert! Distress Detected at {microphone_number}!\n\n{distress_type}.\n\nCheck them now!"

#         # Send SMS using Twilio
#         message = client.messages.create(
#             body=sms_message,
#             from_=twilio_phone_number,
#             to=recipient_number
#         )

#         # Print a success message and message SID
#         print(f"SMS sent successfully to {recipient_number}. Message SID: {message.sid}")

#         # Emit socket message indicating successful SMS sending
#         socketio.emit('sms_status', {'status': 'success', 'message_sid': message.sid})

#     except Exception as e:
#         print("Error sending SMS:", e)
#         # Emit socket message indicating failure in SMS sending
#         socketio.emit('sms_status', {'status': 'failure', 'error_message': str(e)})

# Function to get the SMS message based on distress type
def get_sms_message(distress_type, microphone_number):
    # Modify this function to customize SMS messages based on distress types
    if distress_type == "Chicken is Disturbed":
        return f"Alert! Distress Detected at {microphone_number}!\n\nCHICKEN IS DISTURBED.\n\nNote: Sudden changes, overcrowding, or social disruptions might be causing stress. Check the environment, ensure secure housing, and provide a calming atmosphere. Remember, maintaining consistency and addressing distress signs promptly leads to healthier and happier chickens. Check them now!"
    elif distress_type == "Chicken is in Danger":
        return f"Alert! Distress Detected at {microphone_number}!\n\nCHICKEN IS IN DANGER.\n\nNote: Predators or sudden movements might be causing distress. Double-check your coop's security, reinforce fencing, and employ effective predator management. A safe environment ensures your chickens' well-being. Act swiftly to prevent harm. Check them now!"
    elif distress_type == "Chicken is Threatened":
        return f"Alert! Distress Detected at {microphone_number}!\n\nCHICKEN IS THREATENED.\n\nNote: Aggressive behavior, disruptions, or changes in routine may be causing stress. Provide a calm space, handle them gently, and ensure ample space and resources. Manage aggressive individuals, offer distractions, and maintain stability. Observing and adapting are key to their comfort. Check them now!"

    # Default message if distress type is not recognized
    return "Distress detected. Check the situation."

# Update routes to match those used in the JavaScript and sketch files
@app.route('/receive_audio/<microphone>', methods=['POST'])
def receive_audio(microphone):
    return receive_audio_microphone(microphone, request.get_data())

def receive_audio_microphone(microphone, audio_data):
    try:
        print(f"Received Audio Data {microphone}: Length: {len(audio_data)}")
        
        # Save the received audio as a WAV file
        wav_filename = f'received_audio_{microphone.lower().replace(" ", "_")}.wav'
        save_audio_data_wav(audio_data, wav_filename, microphone, RATE)

        # Process and predict distress using the saved WAV file
        return process_and_predict_distress_wav(wav_filename, microphone)
    except Exception as e:
        print(f"Error receiving audio for {microphone}:", e)
        return jsonify({'status': 'error', 'message': f'Failed to receive audio for {microphone}'})


def process_and_predict_distress_wav(wav_filename, microphone_number):
    try:
        print(f"Processing and predicting distress for {microphone_number}")

        print(f"Sample rate: {RATE}")
        audio_array, _ = librosa.load(wav_filename, sr=RATE)
        print(f"Duration of loaded audio: {len(audio_array) / RATE} seconds")
        print(f"Length of loaded audio array: {len(audio_array)}")

        time_values = np.arange(0, len(audio_array)) / RATE

        # Prepare data for plotting
        waveform_data = {
            'microphone_number': microphone_number,  # Add the microphone number to the data
            'time_values': time_values.tolist(),
            'audio_values': audio_array.tolist(),
        }

        # Compute Mel-frequency Cepstral Coefficients (MFCC)
        mfcc_features = extract_features_wav(audio_array)

        # Prepare MFCC data for plotting
        mfcc_data = {
            'time_values': time_values.tolist(),
            'mfcc_values': mfcc_features.tolist(),
        }

        # Add MFCC data to the waveform_data dictionary
        waveform_data['mfcc_data'] = mfcc_data

        # Compute spectrogram
        f, t, Sxx = signal.spectrogram(audio_array, fs=RATE)

        # Prepare spectrogram data for plotting
        spectrogram_data = {
            'frequencies': f.tolist(),
            'time_values': t.tolist(),
            'spectrogram_values': Sxx.tolist(),
        }

        # Add spectrogram data to the waveform_data dictionary
        waveform_data['spectrogram_data'] = spectrogram_data

        # Compute Mel-frequency Cepstral Coefficients (MFCC)
        features = extract_features_wav(audio_array)

        # Print shapes before prediction
        print(f"Shape of audio_array: {audio_array.shape}")
        print(f"Shape of features before prediction: {features.shape}")

        # Ensure the input shape matches the model's expected shape
        assert features.shape == (1, 20, 38), f"Invalid input shape: {features.shape}"

        prediction = model.predict(features)

        # Print shapes after prediction
        print(f"Shape of prediction output: {prediction.shape}")

        predicted_label = np.argmax(prediction)
        distress_type = classes.get(predicted_label, "Unknown")

        # Skip saving the "Normal" class in the database
        if distress_type not in ["Normal", "Background Noise"]:
            save_distress_log(distress_type, microphone_number)

        # Use the correct global variables
        global general_distress_counts
        global microphone_distress_counts

        general_distress_counts[distress_type] += 1

        # Ensure the key is consistent with your definition (e.g., 'microphone_1')
        microphone_key = f"microphone_{microphone_number.lower().replace(' ', '_')}"

        if microphone_key not in microphone_distress_counts:
            # If the key is not present, create a new entry
            microphone_distress_counts[microphone_key] = {
                "Chicken is Disturbed": 0,
                "Chicken is in Danger": 0,
                "Chicken is Threatened": 0
            }

        microphone_distress_counts[microphone_key][distress_type] += 1

        # Calculate total distress counts
        total_general_distress_counts = sum(general_distress_counts.values())

        total_microphone_distress_counts = {
            "microphone_microphone_1": 0,
            "microphone_microphone_2": 0
        }

        for key, counts in microphone_distress_counts.items():
            total_microphone_distress_counts[key] += sum(counts.values())

        # Emit distress counts to the frontend
        print("Emitting distress_update to the frontend:")
        print(f"  total_general_distress_counts: {total_general_distress_counts}")
        print(f"  total_microphone_distress_counts: {total_microphone_distress_counts}")
        socketio.emit('distress_update', {
            'total_general_distress_counts': total_general_distress_counts,
            'total_microphone_distress_counts': total_microphone_distress_counts
        })

        # Ensure socketio is defined in your code
        print("Emitting general_distress_counts to the frontend:")
        print(f"  general_distress_counts: {general_distress_counts}")
        socketio.emit('general_distress_counts', general_distress_counts)

        # Ensure socketio is defined in your code
        print("Emitting microphone_distress_counts to the frontend:")
        print(f"  microphone_distress_counts: {microphone_distress_counts}")
        socketio.emit('microphone_distress_counts', microphone_distress_counts)

        # Emit distress type to the frontend
        socketio.emit(f'distress_type_{microphone_number.lower().replace(" ", "_")}', {
            'class': distress_type,
        })

        # Emit waveform data to the frontend
        # print("Emitting waveform_data to the frontend:")
        # print(f"  waveform_data: {waveform_data}")
        socketio.emit('waveform_data', waveform_data)

        print(f"Received Audio Data from {microphone_number}")
        print(f"Distress Prediction Class for {microphone_number}: {distress_type}")

        return jsonify({
            'status': 'success',
            f'distress_prediction_{microphone_number.lower().replace(" ", "_")}': {
                'class': distress_type,
            },
            'total_general_distress_counts': general_distress_counts,
            'total_microphone_distress_counts': microphone_distress_counts,
            'waveform_data': waveform_data,  # Include waveform and chromagram data in the response
        })
    except Exception as e:
        print(f"Error processing and predicting distress for {microphone_number}:", e)
        return jsonify({'status': 'error', 'message': f'Failed to process and predict distress for {microphone_number}'})

def extract_features_wav(audio_array):
    # Process audio data directly, as it's already in the correct format
    mfcc_features = librosa.feature.mfcc(y=audio_array, sr=RATE, n_mfcc=20, n_fft=1024)

    # Ensure the number of frames matches the expected shape (20 frames)
    if mfcc_features.shape[1] < MAX_FRAMES:
        mfcc_features = np.pad(mfcc_features, ((0, 0), (0, MAX_FRAMES - mfcc_features.shape[1])), mode='constant')
    else:
        mfcc_features = mfcc_features[:, :MAX_FRAMES]

    mfcc_features = (mfcc_features - np.mean(mfcc_features)) / np.std(mfcc_features)

    # Transpose the array to match the expected shape (20 frames, 38 coefficients)
    mfcc_features = mfcc_features.T

    # Ensure the number of frames matches the expected shape (20 frames)
    if mfcc_features.shape[0] < 20:
        mfcc_features = np.pad(mfcc_features, ((0, 20 - mfcc_features.shape[0]), (0, 0)), mode='constant')
    else:
        mfcc_features = mfcc_features[:20, :]

    # Ensure the number of coefficients matches the expected shape (38 coefficients)
    if mfcc_features.shape[1] < 38:
        mfcc_features = np.pad(mfcc_features, ((0, 0), (0, 38 - mfcc_features.shape[1])), mode='constant')
    else:
        mfcc_features = mfcc_features[:, :38]

    # Add an extra dimension for the batch size
    mfcc_features = mfcc_features[np.newaxis, :, :]

    return mfcc_features

def save_audio_data_wav(audio_data, wav_filename, microphone, RATE):
    received_audios_folder = 'received_audios'
    os.makedirs(received_audios_folder, exist_ok=True)

    full_wav_filename = os.path.join(received_audios_folder, f'received_audio_{microphone.lower().replace(" ", "_")}.wav')

    # Ensure the length of audio_data is a multiple of the element size (1 byte for 8-bit PCM)
    if len(audio_data) % 1 != 0:
        audio_data = audio_data[:-1]

    with wave.open(full_wav_filename, 'wb') as audio_file:
        audio_file.setnchannels(1)  # 1 channel for mono audio
        audio_file.setsampwidth(1)  # 1 byte per sample for 8-bit PCM
        audio_file.setframerate(RATE)
        audio_file.writeframes(audio_data)

    # Load the saved audio file to get the correct duration
    audio_array, _ = librosa.load(full_wav_filename, sr=RATE)
    
    # Calculate the duration based on the length of the loaded audio array and the sample rate
    duration = len(audio_array) / RATE
    print(f"Duration of {full_wav_filename}: {duration} seconds")

    return full_wav_filename

def save_distress_log(distress_type, microphone_number):
    try:
        current_datetime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        query = 'INSERT INTO history_log (datetime, distress_type, microphone_number) VALUES (?, ?, ?)'
        values = (current_datetime, distress_type, microphone_number)

        # Use a context manager to handle the connection
        with sqlite3.connect(app.config['DATABASE_DISTRESS']) as conn:
            cursor = conn.cursor()
            cursor.execute(query, values)
            conn.commit()

            # Fetch all history logs after saving the new log
            cursor.execute('SELECT id, datetime, distress_type, microphone_number FROM history_log ORDER BY id DESC LIMIT 30')
            history_logs = [{'id': row[0], 'datetime': row[1], 'distress_type': row[2], 'microphone_number': row[3]} for row in cursor.fetchall()]

            # # Send SMS using Twilio and emit socket message
            # send_twilio_sms(distress_type, microphone_number, fixed_recipient_number)

        socketio.emit('history_logs_update', {'history_logs': history_logs})

    except Exception as e:
        print("Error saving distress log:", e)


@socketio.on('delete_record')
def handle_delete_record(data):
    record_id = data.get('recordId')
    if record_id is not None:
        try:
            query = 'DELETE FROM history_log WHERE id = ?'
            values = (record_id,)

            with sqlite3.connect(app.config['DATABASE']) as conn:
                cursor = conn.cursor()
                cursor.execute(query, values)
                conn.commit()

                # Fetch all history logs after deleting the record
                cursor.execute('SELECT id, datetime, distress_type, microphone_number FROM history_log ORDER BY id DESC LIMIT 10')
                history_logs = [{'id': row[0], 'datetime': row[1], 'distress_type': row[2], 'microphone_number': row[3]} for row in cursor.fetchall()]

            socketio.emit('history_logs_update', {'history_logs': history_logs})

        except Exception as e:
            print(f"Error deleting record {record_id}:", e)

@socketio.on('clear_all_logs')
def handle_clear_all_logs():
    try:
        query = 'DELETE FROM history_log'

        with sqlite3.connect(app.config['DATABASE']) as conn:
            cursor = conn.cursor()
            cursor.execute(query)
            conn.commit()

            # Fetch all history logs after clearing all logs
            cursor.execute('SELECT id, datetime, distress_type, microphone_number FROM history_log ORDER BY id DESC LIMIT 10')
            history_logs = [{'id': row[0], 'datetime': row[1], 'distress_type': row[2], 'microphone_number': row[3]} for row in cursor.fetchall()]

        socketio.emit('history_logs_update', {'history_logs': history_logs})

    except Exception as e:
        print("Error clearing all logs:", e)

def get_db_calendar():
    db = getattr(g, '_database_calendar', None)
    if db is None:
        db = g._database_calendar = sqlite3.connect(app.config['DATABASE_CALENDAR'])
    return db

def close_db_calendar(e=None):
    db = getattr(g, '_database_calendar', None)
    if db is not None:
        db.close()

@app.teardown_appcontext
def teardown_db_calendar(exception):
    close_db_calendar()

def init_db_calendar():
    with app.app_context():
        db = get_db_calendar()
        cursor = db.cursor()

        cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='calendar_data'")
        table_exists = cursor.fetchone()

        if not table_exists:
            cursor.execute('''
                CREATE TABLE calendar_data (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    observer_name TEXT NOT NULL,
                    date TEXT NOT NULL,
                    time TEXT NOT NULL,
                    location TEXT NOT NULL,
                    chicken_name TEXT,
                    breed TEXT,
                    age TEXT,
                    color TEXT,
                    general_appearance TEXT,
                    comb TEXT,
                    wattles TEXT,
                    beak TEXT,
                    eyes TEXT,
                    nostrils TEXT,
                    legs_feet TEXT,
                    activity TEXT,
                    foraging_behavior TEXT,
                    eating_drinking TEXT,
                    respiratory_rate TEXT,
                    abnormal_discharges TEXT,
                    signs_of_injury_illness TEXT,
                    parasites TEXT,
                    egg_production TEXT,
                    additional_notes TEXT
                )
            ''')

        db.commit()

if not os.path.exists('calendar_data.db'):
    init_db_calendar()

@app.route('/save_calendar_event', methods=['POST'])
def save_calendar_event():
    try:
        db = get_db_calendar()
        observer_name = request.form.get('observerName')
        date = request.form.get('date')
        time = request.form.get('time')
        location = request.form.get('location')
        chicken_name = request.form.get('chickenName')
        breed = request.form.get('breed')
        age = request.form.get('age')
        color = request.form.get('color')
        general_appearance = request.form.get('generalAppearance')
        comb = request.form.get('comb')
        wattles = request.form.get('wattles')
        beak = request.form.get('beak')
        eyes = request.form.get('eyes')
        nostrils = request.form.get('nostrils')
        legs_feet = request.form.get('legsFeet')
        activity = request.form.get('activity')
        foraging_behavior = request.form.get('foragingBehavior')
        eating_drinking = request.form.get('eatingDrinking')
        respiratory_rate = request.form.get('respiratoryRate')
        abnormal_discharges = request.form.get('abnormalDischarges')
        signs_of_injury_illness = request.form.get('signsOfInjuryIllness')
        parasites = request.form.get('parasites')
        egg_production = request.form.get('eggProduction')
        additional_notes = request.form.get('additionalNotes')

        if not observer_name or not date or not time or not location:
            return jsonify({'status': 'error', 'message': 'Observer name, date, time, and location are required'}), 400

        query = '''
        INSERT INTO calendar_data (observer_name, date, time, location, chicken_name, breed, age, color, general_appearance,
                                comb, wattles, beak, eyes, nostrils, legs_feet, activity, foraging_behavior, eating_drinking,
                                respiratory_rate, abnormal_discharges, signs_of_injury_illness, parasites, egg_production, additional_notes)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        '''

        # Check for existing records with the same date and time
        existing_record_query = 'SELECT id FROM calendar_data WHERE date=? AND time=?'
        existing_record = db.execute(existing_record_query, (date, time)).fetchone()

        if existing_record:
            return jsonify({'status': 'error', 'message': 'Observation already exists for the specified date and time'}), 400

        db.execute(query, (
            observer_name, date, time, location, chicken_name, breed, age, color, general_appearance,
            comb, wattles, beak, eyes, nostrils, legs_feet, activity, foraging_behavior, eating_drinking,
            respiratory_rate, abnormal_discharges, signs_of_injury_illness, parasites, egg_production, additional_notes
        ))

        # Commit the changes to the database
        db.commit()

        socketio.emit('new_observation', {
            'observer_name': observer_name,
            'date': date,
            'time': time,
            'location': location,
            'chicken_name': chicken_name,
            'breed': breed,
            'age': age,
            'color': color,
            'general_appearance': general_appearance,
            'comb': comb,
            'wattles': wattles,
            'beak': beak,
            'eyes': eyes,
            'nostrils': nostrils,
            'legs_feet': legs_feet,
            'activity': activity,
            'foraging_behavior': foraging_behavior,
            'eating_drinking': eating_drinking,
            'respiratory_rate': respiratory_rate,
            'abnormal_discharges': abnormal_discharges,
            'signs_of_injury_illness': signs_of_injury_illness,
            'parasites': parasites,
            'egg_production': egg_production,
            'additional_notes': additional_notes
        })

        app.logger.info('Observation saved successfully: %s', request.form)

        return jsonify({'status': 'success', 'message': 'Observation saved successfully'}), 200

    except Exception as e:
        # Log the exception with additional details
        app.logger.error('Error saving observation: %s', e, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Internal Server Error'}), 500

@app.route('/get_all_observations', methods=['GET'])
def get_all_observations():
    try:
        db = get_db_calendar()
        query = 'SELECT * FROM calendar_data ORDER BY id DESC'
        observations = db.execute(query).fetchall()

        observation_list = []
        for observation in observations:
            observation_dict = {
                'id': observation[0],
                'observer_name': observation[1],
                'date': observation[2],
                'time': observation[3],
                'location': observation[4],
                'chicken_name': observation[5],
                'breed': observation[6],
                'age': observation[7],
                'color': observation[8],
                'general_appearance': observation[9],
                'comb': observation[10],
                'wattles': observation[11],
                'beak': observation[12],
                'eyes': observation[13],
                'nostrils': observation[14],
                'legs_feet': observation[15],
                'activity': observation[16],
                'foraging_behavior': observation[17],
                'eating_drinking': observation[18],
                'respiratory_rate': observation[19],
                'abnormal_discharges': observation[20],
                'signs_of_injury_illness': observation[21],
                'parasites': observation[22],
                'egg_production': observation[23],
                'additional_notes': observation[24],
            }
            observation_list.append(observation_dict)

        return jsonify({'status': 'success', 'observations': observation_list}), 200

    except Exception as e:
        # Log the exception with additional details
        app.logger.error('Error fetching observations: %s', e, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Internal Server Error'}), 500

@app.route('/delete_observation', methods=['POST'])
def delete_observation():
    try:
        record_id = request.form.get('id')
        if record_id is not None:
            query = 'DELETE FROM calendar_data WHERE id = ?'
            values = (record_id,)

            with sqlite3.connect(app.config['DATABASE_CALENDAR']) as conn:
                cursor = conn.cursor()
                cursor.execute(query, values)
                conn.commit()

                # You may want to fetch all observations after deleting the record
                cursor.execute('SELECT * FROM calendar_data ORDER BY id DESC')
                observations = cursor.fetchall()

                observation_list = []
                for observation in observations:
                    observation_dict = {
                        'id': observation[0],
                        'observer_name': observation[1],
                        'date': observation[2],
                        'time': observation[3],
                        'location': observation[4],
                        'chicken_name': observation[5],
                        'breed': observation[6],
                        'age': observation[7],
                        'color': observation[8],
                        'general_appearance': observation[9],
                        'comb': observation[10],
                        'wattles': observation[11],
                        'beak': observation[12],
                        'eyes': observation[13],
                        'nostrils': observation[14],
                        'legs_feet': observation[15],
                        'activity': observation[16],
                        'foraging_behavior': observation[17],
                        'eating_drinking': observation[18],
                        'respiratory_rate': observation[19],
                        'abnormal_discharges': observation[20],
                        'signs_of_injury_illness': observation[21],
                        'parasites': observation[22],
                        'egg_production': observation[23],
                        'additional_notes': observation[24],
                    }
                    observation_list.append(observation_dict)

                socketio.emit('observations_update', {'observations': observation_list})

                return jsonify({'status': 'success', 'message': 'Observation deleted successfully'}), 200

        return jsonify({'status': 'error', 'message': 'Record ID is missing'}), 400

    except Exception as e:
        app.logger.error('Error deleting observation: %s', e, exc_info=True)
        return jsonify({'status': 'error', 'message': 'Internal Server Error'}), 500

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000, use_reloader=False)