var DistressManager = (function () {
  var chartColors = [
    "rgba(255, 99, 132, 0.9)",
    "rgba(54, 162, 235, 0.9)",
    "rgba(255, 206, 86, 0.9)",
  ];

  var socket = io();
  socket.on('connect', function () {
    console.log('Socket.IO connected');
  });

  var totalGeneralDistress = { count: 0 };
  var totalMicrophoneDistress = { microphone_1: 0, microphone_2: 0 };

  var pieChart; // Declare pieChart variable outside the function

  function createPieChart(data) {
    var ctxElement = document.getElementById("pieChart");

    if (!ctxElement) {
      console.error("Element with id 'pieChart' not found in the document.");
      return;
    }

    var ctx = ctxElement.getContext("2d");

    if (!ctx) {
      console.error("Failed to get 2D context for 'pieChart'.");
      return;
    }

    // Destroy the existing chart, if any
    if (pieChart) {
      pieChart.destroy();
    }

    pieChart = new Chart(ctx, {
      type: "pie",
      data: {
        labels: Object.keys(data),
        datasets: [
          {
            data: Object.values(data),
            backgroundColor: chartColors,
          },
        ],
      },
      options: {
        responsive: true,
      },
    });
  }

  function displayHorizontalBarGraph(data, microphone) {
    var ctxElement = document.getElementById(`HorizontalBarGraph_${microphone}`);

    if (!ctxElement) {
      console.error(`Element with id 'HorizontalBarGraph_${microphone}' not found in the document.`);
      return;
    }

    var ctx = ctxElement.getContext("2d");

    if (!ctx) {
      console.error(`Failed to get 2D context for 'HorizontalBarGraph_${microphone}'.`);
      return;
    }

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    var totalCount = Object.values(data).reduce((a, b) => a + b, 0);
    var barWidth = 650;
    var barHeight = 20;
    var ySpacing = 20;
    var currentY = 30;
    var colorIndex = 0;

    for (var distressType in data) {
      var count = data[distressType];
      var percentage = ((count / totalCount) * 100).toFixed(2);
      var color = chartColors[colorIndex++ % chartColors.length];

      ctx.fillStyle = color;
      ctx.fillRect(150, currentY, (percentage / 100) * barWidth, barHeight);

      ctx.font = '12px Arial';
      ctx.fillStyle = 'black';
      ctx.textAlign = 'left';
      ctx.textBaseline = 'middle';
      ctx.fillText(distressType, 10, currentY + barHeight / 2);

      ctx.fillStyle = 'black';
      ctx.textAlign = 'center';
      ctx.fillText(percentage + '%', 150 + (percentage / 100) * barWidth / 2, currentY + barHeight / 2);

      currentY += barHeight + ySpacing;
    }
  }

  function displayTotalDistressValues() {
    var totalDistressRow = document.getElementById("totalDistressRow");

    if (!totalDistressRow) {
      console.error("Element with id 'totalDistressRow' not found in the document.");
      return;
    }

    totalDistressRow.innerHTML = `
      <div class="distress-card">
        <p class="distress-label">GENERAL:</p>
        <p class="distress-number">${totalGeneralDistress.count}</p>
      </div>
      <div class="distress-card">
        <p class="distress-label">CAGE 1:</p>
        <p class="distress-number">${totalMicrophoneDistress.microphone_1}</p>
      </div>
      <div class="distress-card">
        <p class="distress-label">CAGE 2:</p>
        <p class="distress-number">${totalMicrophoneDistress.microphone_2}</p>
      `;
  }

  socket.on("distress_update", function (data) {
    console.log('Distress Update:', data);

    totalGeneralDistress.count = data.total_general_distress_counts;
    totalMicrophoneDistress.microphone_1 = data.total_microphone_distress_counts.microphone_microphone_1;
    totalMicrophoneDistress.microphone_2 = data.total_microphone_distress_counts.microphone_microphone_2;

    displayTotalDistressValues();
  });

  socket.on("general_distress_counts", function (data) {
    console.log('Distress Counts:', data);
    createPieChart(data);
  });

  socket.on("microphone_distress_counts", function (data) {
    console.log('Microphone Distress Counts:', data);

    if (data && chartColors) {
      for (var microphone in data) {
        displayHorizontalBarGraph(data[microphone], microphone);
      }
    }
  });

  return {};
})();
