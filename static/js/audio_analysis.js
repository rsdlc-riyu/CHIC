document.addEventListener('DOMContentLoaded', function () {
  // Connect to the Socket.IO server
  const socket = io.connect('http://192.168.254.121:5000');

  // Create Plotly instances for waveform, spectrogram, and MFCC for both microphones
  const waveformDiv1 = document.getElementById('waveform_microphone_1');
  let plotWaveform1;
  const spectrogramDiv1 = document.getElementById('spectrogram_microphone_1');
  let plotSpectrogram1;
  const mfccDiv1 = document.getElementById('mfcc_microphone_1');
  let plotMFCC1;

  const waveformDiv2 = document.getElementById('waveform_microphone_2');
  let plotWaveform2;
  const spectrogramDiv2 = document.getElementById('spectrogram_microphone_2');
  let plotSpectrogram2;
  const mfccDiv2 = document.getElementById('mfcc_microphone_2');
  let plotMFCC2;

  // Listen for waveform, spectrogram, and MFCC data updates for both microphones
  socket.on('waveform_data', function (data) {
    console.log('Waveform Data:', data);

    // Assume the data includes 'microphone_number'
    const microphoneNumber = data.microphone_number;

    // Extract time, audio, and MFCC values
    const timeValues = data.time_values;
    const audioValues = data.audio_values;
    const mfccValues = data.mfcc_data ? data.mfcc_data.mfcc_values : null;

    // Convert the microphone number to lowercase for consistency
    const formattedMicrophoneNumber = microphoneNumber.toLowerCase();

    // Update the waveform plot
    const waveformDiv = document.getElementById(`waveform_${formattedMicrophoneNumber}`);
    let plotWaveform;

    if (plotWaveform) {
      Plotly.extendTraces(waveformDiv, {
        x: [timeValues],
        y: [audioValues]
      }, [0]);
    } else {
      plotWaveform = Plotly.newPlot(waveformDiv, [{
        x: timeValues,
        y: audioValues,
        type: 'scatter',
        mode: 'lines',
        name: `Waveform ${formattedMicrophoneNumber}`
      }], {
        title: `Waveform for ${formattedMicrophoneNumber}`,
        xaxis: { title: 'Time' },
        yaxis: { title: 'Amplitude' }
      });
    }

    // Update the spectrogram plot
    const spectrogramDiv = document.getElementById(`spectrogram_${formattedMicrophoneNumber}`);
    let plotSpectrogram;

    if (plotSpectrogram) {
      console.log('Updating Spectrogram plot with data:', { z: [data.spectrogram_data.spectrogram_values] });
      Plotly.update(spectrogramDiv, { z: [data.spectrogram_data.spectrogram_values] });
    } else {
      console.log('Creating new Spectrogram plot with data:', { z: [data.spectrogram_data.spectrogram_values] });
      plotSpectrogram = Plotly.newPlot(spectrogramDiv, [{
        z: data.spectrogram_data.spectrogram_values,
        type: 'heatmap',
      }], {
        title: `Spectrogram for ${formattedMicrophoneNumber}`,
        xaxis: { title: 'Time' },
        yaxis: { title: 'Frequency' }
      });
    }

    // Update the MFCC plot
const mfccDiv = document.getElementById(`mfcc_${formattedMicrophoneNumber}`);
let plotMFCC;

if (mfccValues) {
  // Flatten the subarrays in mfccValues
  const flattenedMFCCValues = mfccValues.flat();

  if (plotMFCC) {
    console.log('Updating MFCC plot with data:', { x: [timeValues], y: [flattenedMFCCValues] });
    Plotly.update(mfccDiv, { x: [timeValues], y: [flattenedMFCCValues] });
  } else {
    console.log('Creating new MFCC plot with data:', { x: timeValues, y: flattenedMFCCValues });
    plotMFCC = Plotly.newPlot(mfccDiv, [{
      x: timeValues,
      y: flattenedMFCCValues,
      type: 'scatter',
      mode: 'lines',
      name: `MFCC ${formattedMicrophoneNumber}`
    }], {
      title: `MFCC for ${formattedMicrophoneNumber}`,
      xaxis: { title: 'Time' },
      yaxis: { title: 'MFCC Value' }
    });
  }
}
  });

  // Function to get initial spectrogram data (modify as needed)
  function getInitialSpectrogramData() {
    // Example data, modify as needed
    const spectrogramData = [{
      z: [[0]],
      type: 'heatmap',
    }];
    return spectrogramData;
  }

   // Listen for distress prediction updates for each microphone
   socket.on('distress_type_microphone_1', function (data) {
    console.log('Distress Type for Microphone 1:', data);
    document.getElementById('prediction_microphone_1').innerHTML = data.class;
  });

  socket.on('distress_type_microphone_2', function (data) {
    console.log('Distress Type for Microphone 2:', data);
    document.getElementById('prediction_microphone_2').innerHTML = data.class;
  });


  // Listen for SMS status updates
  socket.on('sms_status', function (data) {
    console.log('SMS Status:', data);

    // Display the SMS status on the frontend
    const statusElement = document.getElementById('sms_status');

    if (data.status === 'success') {
      const successMessage = document.createElement('div');
      successMessage.innerHTML = '<p class="flash-message" style="font-size: 20px; text-align: center; color: green;">SMS NOTIFICATION WAS SENT SUCCESSFULLY!</p>';

      const messageSid = document.createElement('p');
      messageSid.innerHTML = `Message SID: ${data.message_sid}`;
      messageSid.style.textAlign = 'center';
      messageSid.style.fontSize = '18px'; // Adjust the font size as needed
      messageSid.style.color = 'green'; // Optional: Set a color for the message SID

      statusElement.innerHTML = ''; // Clear previous content
      statusElement.appendChild(successMessage);
      statusElement.appendChild(messageSid);
    } else {
      const errorMessage = document.createElement('div');
      errorMessage.innerHTML = `<p class="flash-message" style="font-size: 18px; text-align: center; color: red;">Error sending SMS: ${data.error_message}</p>`;

      statusElement.innerHTML = ''; // Clear previous content
      statusElement.appendChild(errorMessage);
    }
  });
});
