document.addEventListener('DOMContentLoaded', function () {
    var observationDates = [];
    var socket = io.connect('http://192.168.254.121:5000');

    // Function to get the current date and time
    function getCurrentDateTime() {
        return moment(); // This will give you the current date and time
    }

    // Function to check if the observation matches the search term
    function matchesSearchTerm(observation, searchTerm) {
        for (var key in observation) {
            if (observation.hasOwnProperty(key) && observation[key].toString().toLowerCase().includes(searchTerm.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    // Function to open the observation modal
    function openObservationFormModal() {
        $('#observationForm')[0].reset();
        var currentDate = getCurrentDateTime();
        openObservationModal(currentDate);

        // Add a calendar button for datepicker
        $('#date').datepicker({
            dateFormat: 'mm/dd/yy',
            onSelect: function (dateText, inst) {
                var formattedDate = moment(dateText, 'MM/DD/YYYY').format('MM/DD/YYYY');
                $(this).val(formattedDate);
            }
        });

        // Add timepicker for the time field
        $('#time').timepicker({
            timeFormat: 'hh:mm tt',
            interval: 15,
            scrollbar: true
        });

        // Add glow effect on click
        $('#date, #time').on('click', function () {
            $(this).addClass('glow');
        });

        // Remove glow effect on blur
        $('#date, #time').on('blur', function () {
            $(this).removeClass('glow');
        });

        $('#observationModal').modal('show');
    }

    // Function to open the observation modal - defined before usage
    function openObservationModal(date) {
        $('#observationForm')[0].reset();
        $('#date').val(date.format('MM/DD/YYYY'));
        $('#time').val(date.format('hh:mm A'));

        // Add a calendar button for datepicker
        $('#date').datepicker({
            dateFormat: 'mm/dd/yy',
            onSelect: function (dateText, inst) {
                var formattedDate = moment(dateText, 'MM/DD/YYYY').format('MM/DD/YYYY');
                $(this).val(formattedDate);
            }
        });

        // Add timepicker for the time field
        $('#time').timepicker({
            timeFormat: 'hh:mm tt',
            interval: 15,
            scrollbar: true
        });

        // Add glow effect on click
        $('#date, #time').on('click', function () {
            $(this).addClass('glow');
        });

        // Remove glow effect on blur
        $('#date, #time').on('blur', function () {
            $(this).removeClass('glow');
        });

        $('#observationModal').modal('show');
    }

    // Bind click event to the "Add Observation" button
    $('#observation_button').on('click', function () {
        openObservationFormModal();
    });

    function showObservationDetailsDialog(formData) {
        $('#observationModal').modal('hide');

        var modalHtml = `
            <div class="modal fade" id="observationDetailsModal" tabindex="-1" role="dialog" aria-labelledby="observationDetailsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="observationDetailsModalLabel">Observation Details</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ${createObservationDetailsHtml(formData)}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>`;

        $('body').append(modalHtml);

        $('#observationDetailsModal').modal('show');

        $('#observationDetailsModal').on('hidden.bs.modal', function () {
            saveObservation(formData);
        });
    }

    function createObservationDetailsHtml(formData) {
        var content = '';
        for (var key in formData) {
            var label = key.charAt(0).toUpperCase() + key.slice(1).replace(/_/g, ' ');
            var value = formData[key] || 'N/A';
            content += `<p><strong>${label}:</strong> ${value}</p>`;
        }
        return content;
    }

    function saveObservation(formData) {
        $.ajax({
            url: '/save_calendar_event',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                observationDates.push({
                    title: 'Observation',
                    start: formData.date + ' ' + formData.time,
                    allDay: true
                });
                addObservationMarkers();
                populateObservationTable();
            },
            error: function (error) {
                console.log('Error saving observation:', error);
            }
        });
    }

    $("#observationForm").submit(function (event) {
        event.preventDefault();

        var formData = {
            observerName: $('#observerName').val(),
            date: $('#date').val(),
            time: $('#time').val(),
            location: $('#location').val(),
            chickenName: $('#chickenName').val(),
            breed: $('#breed').val(),
            age: $('#age').val(),
            color: $('#color').val(),
            generalAppearance: $('#generalAppearance').val(),
            comb: $('#comb').val(),
            wattles: $('#wattles').val(),
            beak: $('#beak').val(),
            eyes: $('#eyes').val(),
            nostrils: $('#nostrils').val(),
            legsFeet: $('#legsFeet').val(),
            activity: $('#activity').val(),
            foragingBehavior: $('#foragingBehavior').val(),
            eatingDrinking: $('#eatingDrinking').val(),
            respiratoryRate: $('#respiratoryRate').val(),
            abnormalDischarges: $('#abnormalDischarges').val(),
            signsOfInjuryIllness: $('#signsOfInjuryIllness').val(),
            parasites: $('#parasites').val(),
            eggProduction: $('#eggProduction').val(),
            additionalNotes: $('#additionalNotes').val()
        };

        if (!moment(formData.date, 'MM/DD/YYYY', true).isValid()) {
            console.error('Invalid date format');
            return;
        }

        showObservationDetailsDialog(formData);
        console.log('Form Data:', formData);
    });

    function populateObservationTable(searchTerm = '') {
        $('#observationTable tbody').empty();

        $.ajax({
            url: '/get_all_observations',
            type: 'GET',
            success: function (response) {
                if (response.status === 'success') {
                    if (Array.isArray(response.observations)) {
                        response.observations.forEach(function (observation) {
                            if (matchesSearchTerm(observation, searchTerm)) {
                                $('#observationTable tbody').append(`
                                    <tr>
                                        <td>${observation.observer_name}</td>
                                        <td>${observation.date}</td>
                                        <td>${observation.time}</td>
                                        <td>${observation.location}</td>
                                        <td>${observation.chicken_name}</td>
                                        <td>${observation.breed}</td>
                                        <td>${observation.age}</td>
                                        <td>${observation.color}</td>
                                        <td>${observation.general_appearance}</td>
                                        <td>${observation.comb}</td>
                                        <td>${observation.wattles}</td>
                                        <td>${observation.beak}</td>
                                        <td>${observation.eyes}</td>
                                        <td>${observation.nostrils}</td>
                                        <td>${observation.legs_feet}</td>
                                        <td>${observation.activity}</td>
                                        <td>${observation.foraging_behavior}</td>
                                        <td>${observation.eating_drinking}</td>
                                        <td>${observation.respiratory_rate}</td>
                                        <td>${observation.abnormal_discharges}</td>
                                        <td>${observation.signs_of_injury_illness}</td>
                                        <td>${observation.parasites}</td>
                                        <td>${observation.egg_production}</td>
                                        <td>${observation.additional_notes}</td>
                                        <td>
                                        <button style="background-color: #6190E8; color: white;" class="btn delete-btn" data-record-id="${observation.id}">Delete</button>
                                        </td>
                                    </tr>
                                `);
                            }
                        });
                    } else {
                        console.error('Observations is not an array:', response.observations);
                    }
                } else {
                    console.error('Error fetching observations:', response.message);
                }
            },
            error: function (error) {
                console.error('Error fetching observations:', error);
            }
        });
    }

    $('#observationTable').on('click', '.delete-btn', function () {
        var observationId = $(this).data('record-id');
        var deleteButton = $(this); // Store the reference

        socket.emit('delete_record', { recordId: observationId });

        $.ajax({
            url: '/delete_observation',
            type: 'POST',
            data: { id: observationId },
            dataType: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    // Use the stored reference to remove the row
                    deleteButton.closest('tr').remove();
                } else {
                    console.error('Error deleting observation:', response.message);
                }
            },
            error: function (error) {
                console.error('Error deleting observation:', error);
            }
        });
    });

    populateObservationTable();
});