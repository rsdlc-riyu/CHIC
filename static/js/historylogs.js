var socket = io.connect('http://' + document.domain + ':' + location.port);

socket.on('connect', function () {
    console.log('Connected to server');
});

socket.on('history_logs_update', function (data) {
    console.log('Received history logs update:', data);
    updateHistoryLogs(data.history_logs);
});

function updateHistoryLogs(historyLogs) {
    console.log('Updating history logs:', historyLogs);
    var historyLogsContainer = $('#history-logs');
    var searchNotFound = $('#search-not-found');
    historyLogsContainer.empty();

    var searchTerm = $('#search-input').val().toLowerCase();
    var selectedDistressClasses = getSelectedDistressClasses();
    var matchFound = false;

    for (var i = 0; i < historyLogs.length; i++) {
        var log = historyLogs[i];

        // Check if the log's distress class is in the selected distress classes
        if (selectedDistressClasses.includes(log.distress_type)) {
            var logEntry = `<tr>
                <td>${log.id}</td>
                <td>${log.datetime}</td>
                <td>${log.distress_type}</td>
                <td>${log.microphone_number}</td>
                <td><button class="delete-btn" style="background-color: #6190E8; color: white; border-radius: 5px; font-size: 12px; padding: 8px 12px;" data-record-id="${log.id}">Delete</button></td>
                </tr>`;

            if (searchTerm && !matchFound) {
                // Check if the current log matches the search term
                matchFound = checkMatch(log, searchTerm);
            }

            historyLogsContainer.append(logEntry);
        }
    }

    // Display/hide "Sorry. No Search Found" message
    searchNotFound.toggle(!matchFound);
    console.log('History logs updated successfully');

    // Use event delegation for dynamic elements (delete buttons)
    $('.delete-btn').on('click', function () {
        var recordId = $(this).data('record-id');
        deleteRecord(recordId);
    });
}

function getSelectedDistressClasses() {
    var selectedDistressClasses = [];
    $('.distress-checkbox:checked').each(function () {
        selectedDistressClasses.push($(this).val());
    });
    return selectedDistressClasses;
}

function checkMatch(log, searchTerm) {
    var match = false;

    // Check if the search term matches any column in the log
    for (var prop in log) {
        if (log.hasOwnProperty(prop) && typeof log[prop] === 'string' && log[prop].toLowerCase().includes(searchTerm)) {
            match = true;
            break;
        }
    }

    return match;
}

function deleteRecord(recordId) {
    // Show confirmation pop-up before deleting
    if (confirm("Are you sure you want to delete this record?")) {
        // Implement the logic to delete the record with the given ID
        socket.emit('delete_record', { recordId: recordId });
    }
}

// Search functionality
$(document).ready(function () {
    $('#searchForm').submit(function (e) {
        e.preventDefault();
        var searchTerm = $('#search-input').val().toLowerCase();
        var rows = $('#history-logs tr');

        rows.filter(':not(:first-child)').each(function () {
            var row = $(this);
            var match = false;

            row.find('td').each(function () {
                if ($(this).text().toLowerCase().includes(searchTerm)) {
                    match = true;
                    return false;  // Break the loop if a match is found in this row
                }
            });

            row.toggle(match);
        });
    });

    $('#clearSearchButton').click(function () {
        $('#search-input').val('');
        $('#searchForm').submit();
    });

    // Add event listener for the clear all logs button
    $('#clearAllLogsButton').click(function () {
        // Show confirmation pop-up before clearing all logs
        if (confirm("Are you sure you want to clear all logs?")) {
            socket.emit('clear_all_logs');
        }
    });

    // Add event listener for the sort button
    $('#sortButton').click(function () {
        sortRecords();
    });
    
    function sortRecords() {
        var fromDate = new Date($('#fromDate').val());
        var toDate = new Date($('#toDate').val());
    
        if (fromDate > toDate) {
            alert('Invalid date range. "From" date should be before or equal to "To" date.');
            return;
        }
    
        var historyLogs = [];  // Your original data or fetch from server
    
        // Filter records based on date range
        var filteredLogs = historyLogs.filter(function (log) {
            var logDate = new Date(log.datetime);
            return logDate >= fromDate && logDate <= toDate;
        });
    
        updateHistoryLogs(filteredLogs);
    }    
});
