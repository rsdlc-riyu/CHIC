// Optional: Adding functionality to handle checkbox change
const toggleSwitches = document.querySelectorAll('.distress-toggle');

toggleSwitches.forEach(function (toggleSwitch) {
  toggleSwitch.addEventListener('change', function () {
    // Perform actions when the toggle switches change state
    // For example: console.log('Checkbox changed:', this.checked);
  });
});
